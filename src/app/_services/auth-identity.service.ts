import { Injectable } from '@angular/core';
import { LocalStorage } from './local-storage.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

export class User {
  username: any;
  email: any;
  token: any;
  role: any;
  allowed_resources: any;
}

@Injectable()
export class AuthIdentityService {
  public loader = new BehaviorSubject<boolean>(false);
  user: User = new User;
  public authKey:string = Md5.hashStr('ludo', false).toString();;
  public LocalStorage: LocalStorage = new LocalStorage();

  constructor( ) {}

  isLoggedIn(): boolean {
    return this.LocalStorage.getItem(this.authKey) !== null;
    // return this.LocalStorage.getItem('currentUser') !== null;
  }

  logOut(router?: Router) {
    this.LocalStorage.removeItem(this.authKey);
    if (router !== undefined) {
      router.navigate(['/login']);
    }
  }

  setUser(user: any): void {
    this.LocalStorage.removeItem(this.authKey);
    this.LocalStorage.setItem(this.authKey, JSON.stringify(user));
  }

  getIdentity(): any {
    if (!this.isLoggedIn()) {
      return null;
    } else {
      let storedUser: any,
      storedUserObj;

      storedUser = this.LocalStorage.getItem(this.authKey);
      storedUserObj = JSON.parse(storedUser);

      this.user = storedUserObj;

      return this.user;
    }
  }

  setAuthToken(token: string): void {
    this.LocalStorage.setItem('tempAuthToken', token);
  }

  getAuthToken(): any {
    return this.LocalStorage.getItem('tempAuthToken');
  }

  removeAuthToken(): void {
    this.LocalStorage.removeItem('tempAuthToken');
  }

  loader_show() {
    this.loader.next(true);
    return this.loader;
  }

  loader_hide() {
    this.loader.next(false);
    return this.loader;
  }
}
