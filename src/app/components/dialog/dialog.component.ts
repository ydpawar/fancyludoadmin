import { Component, Inject, Injector, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseComponent } from '../commonComponent';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-dialog',
  template: `
  <div class="text-center"><i class="material-icons">info</i></div>
  <h1 mat-dialog-title class="text-center">{{data.title}}</h1>
  <div mat-dialog-content class="text-center">
    <p>Are you sure to ? {{data.title}} </p>
  </div>
  <div mat-dialog-actions class="col-md-12 row" style="justify-content: center;">
      <button class="btn btn-success col-md-4" mat-button [mat-dialog-close]="true">Yes ! Sure</button>
       <button class="btn btn-danger col-md-4" mat-button [mat-dialog-close]="false">No</button>
  </div>`,
  styles: [
    '.material-icons { font-size: 80px;}'
  ]
  
})
export class DialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
  }
}
