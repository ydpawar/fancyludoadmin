import { Component, OnInit, PLATFORM_ID, Injector, NgZone, APP_ID, ChangeDetectorRef, Inject } from '@angular/core';
import { TransferState, makeStateKey, Title, Meta } from '@angular/platform-browser';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { BehaviorSubject } from 'rxjs';
import { AuthIdentityService } from 'app/_services';

declare var jquery: any;
declare var $: any;

@Component({
    selector: 'parent-comp',
    template: ` `,
    providers: []
})

export class BaseComponent {

    public confmDialog = new BehaviorSubject<boolean>(false);

    public activatedRoute: ActivatedRoute;
    public changeRef: ChangeDetectorRef
    public routeUrl: any;
    public titleService: Title;
    public metaService: Meta;
    public platformId: any;
    public appId: any;
    public router: Router;
    public baseUrl: any;
    public formBuilder: FormBuilder;
    public frm: any = FormGroup;
    public location: Location;
    public dialog: MatDialog
    public auth :any = AuthIdentityService;

    constructor(injector: Injector) {
        this.changeRef = injector.get(ChangeDetectorRef);
        this.router = injector.get(Router);
        this.platformId = injector.get(PLATFORM_ID);
        this.appId = injector.get(APP_ID);
        this.titleService = injector.get(Title);
        this.metaService = injector.get(Meta);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.auth = injector.get(AuthIdentityService);
        this.formBuilder = injector.get(FormBuilder);
        this.location = injector.get(Location);
        this.dialog = injector.get(MatDialog);
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.routeUrl = event.urlAfterRedirects;
            }
        });
        
    }

    // *************************************************************//
    // @Purpose : To check server or browser

    // *************************************************************//
    isBrowser() {
        if (isPlatformBrowser(this.platformId)) {
            return true;
        } else {
            return false;
        }
    }

    // *************************************************************//
    // @Purpose : We can use following function to use localstorage
    // *************************************************************//
    setToken(key: any, value: any) {
        if (isPlatformBrowser(this.platformId)) {
            sessionStorage.setItem(key, value);
        }
    }

    // getToken(key:any) {
    //     if (isPlatformBrowser(this.platformId)) {
    //         return sessionStorage.getItem(key);
    //     }
    // }

    removeToken(key: any) {
        if (isPlatformBrowser(this.platformId)) {
            sessionStorage.removeItem(key);
        }
    }

    clearToken() {
        if (isPlatformBrowser(this.platformId)) {
            sessionStorage.clear();
        }
    }

    // *************************************************************//

    // *************************************************************//
    // @Purpose : We can use following function to use Toaster Service.
    // *************************************************************//
    popToast(type: any, title: any) {
        // swal.fire({
        //     position: 'center',
        //     // type: type,
        //     text: title,
        //     showConfirmButton: false,
        //     timer: 3000,
        //     // customClass: 'custom-toaster'
        // });
    }

    back() {
        this.location.back();
    }

    initDataTables(domElId: any) {
        // tslint:disable-next-line:only-arrow-functions
        $(document).ready(function () {
            $('#' + domElId).DataTable({
                'scrollX': true,
                'destroy': true,
                'retrieve': true,
                'stateSave': true,
                'language': {
                    'paginate': {
                        'previous': '<i class=\'mdi mdi-chevron-left\'>',
                        'next': '<i class=\'mdi mdi-chevron-right\'>'
                    }
                },
                'drawCallback': function drawCallback() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            }); // Complex headers with column visibility Datatable
        });
    }

    /****************************************************************************
     @PURPOSE      : To restrict or allow some values in input.
     @PARAMETERS   : $event
     @RETURN       : Boolen
     ****************************************************************************/
    RestrictSpace(e: any) {
        if (e.keyCode == 32) {
            return false;
        } else {
            return true;
        }
    }

    AllowNumbers(e: any) {
        var input;
        if (e.metaKey || e.ctrlKey) {
            return true;
        }
        if (e.which === 32) {
            return false;
        }
        if (e.which === 0) {
            return true;
        }
        if (e.which < 33) {
            return true;
        }
        if (e.which === 43 || e.which === 45) {
            return true;
        }
        if (e.which === 36 || e.which === 35) {
            return true;
        }
        if (e.which === 37 || e.which === 39) {
            return true;
        }
        input = String.fromCharCode(e.which);
        return !!/[\d\s]/.test(input);
    }

    AllowChar(e: any) {
        if ((e.keyCode > 64 && e.keyCode < 91) || (e.keyCode > 96 && e.keyCode < 123) || e.keyCode == 8) {
            return true;
        } else {
            return false;
        }
    }

    /****************************************************************************/
    /****************************************************************************/

    /****************************************************************************/
    // getProfile() {
    //     // const url = this.getToken('ss_pic');
    //     if (url == null || url === ' ') {
    //         return 'assets/images/NoProfile.png';
    //     } else {
    //         return url;
    //     }
    // }

    /****************************************************************************
     //For COOKIE
     /****************************************************************************/
    setCookie(name: any, value: any, days: any) {
        var expires = '';
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = '; expires=' + date.toUTCString();
        }
        document.cookie = name + '=' + (value || '') + expires + '; path=/';
    }

    getCookie(name: any) {
        var nameEQ = name + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    }

    eraseCookie(name: any) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }

    screen_size() {
        $('.screen-detail ').height($(window).height() - ($('header ').outerHeight() + $('.screen-tab ').outerHeight() + $('footer ').outerHeight()));
    }

    getLastChar(id: any) {
        return id.substr(id.length - 6); // => "1"
    }

    showNotification(msg, tpe) {
        let color;
        let icons
        if (tpe == 'Success') {
            color = ['success'];
            icons = ['notifications'];
        } 
        if (tpe == 'Error') {
            color = ['danger'];
            icons = ['warning'];
        }
        $.notify({
            icon: icons,
            message: msg,
            title:tpe
        }, {
            type: color,
            timer: 2000,
            placement: {
                from: 'top',
                align: 'right'
            },
            template: '<div data-notify="container" class="col-xl-3 col-lg-3 col-11 col-sm-3 col-md-3 alert alert-{0} alert-with-icon" role="alert">' +
                '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
                '<i class="material-icons" data-notify="icon">'+ icons +'</i> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    confirmDialog(title): void {
        const dialogRef = this.dialog.open(DialogComponent, {
          width: '500px',
          data: {title: title}
        });
        dialogRef.afterClosed().subscribe(result => {
          this.confmDialog.next(result);
        });
    }

}
