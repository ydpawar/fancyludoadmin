import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthIdentityService } from 'app/_services';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: '/users', title: 'Users',  icon:'person', class: '' },
    { path: '/table', title: 'Table',  icon:'table_view', class: '' },
    { path: '/table-pool', title: 'Table pool',  icon:'content_paste', class: '' },
    { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
    { path: '/coupon', title: 'Coupon',  icon:'collections_bookmark', class: '' },
    { path: '/transaction', title: 'Transaction',  icon:'receipt_long', class: '' },
    
    // { path: '/table-list', title: 'Table List',  icon:'content_paste', class: '' },
    // { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
    // { path: '/typography', title: 'Typography',  icon:'library_books', class: '' },
    // { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  user : any;

  constructor(private router: Router,
    private auth: AuthIdentityService
  ) {
    if(this.auth.isLoggedIn()){
      this.user = this.auth.getIdentity();
    }
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };

  logOut() {
    this.auth.logOut();
    this.router.navigate(['/login']);
  }
}
