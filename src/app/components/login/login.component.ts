import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { LoginService, } from '../../_api/index';
import { Router } from '@angular/router';
import { AuthIdentityService } from './../../_services/index';
// import { ToastrService } from 'ngx-toastr';

declare var $: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [AuthIdentityService]
})

export class LoginComponent implements OnInit, AfterViewInit {
    // @ViewChild('recaptcha', {static: false}) recaptcha: ElementRef;
    // @ViewChild('apklink', {static: false}) apklink: ElementRef;
    model: any = {};
    // public setlogo: string;
    public isreCaptcha: any;
    sitemode: boolean = true;
    disableLogin: boolean = false;
    siteMaintenanceMessage: string = 'Site under maintenance. We will be back shortly!';
    // returnUrl: string;
    captcha: string = '------';
    // systemId: number;
    // apkUrl: string;
    // apkDownloadTag: string;
    authId: any;
    passType = 'password';
    passType1 = 'password';
    passType2 = 'password';

    // tslint:disable-next-line:max-line-length
    constructor(
        private auth: LoginService,
        // private service: UserDataService,
        private route: Router,
        private authIdentity: AuthIdentityService,
        // private toastr: ToastrService
    ) {

    }

    ngOnInit() {
        // this.systemId = Number(window['operatorId']);
        // this.checkSiteMode();
    }

    ngAfterViewInit() { }


    // reloadCaptch(captcha:any) {
    //     this.model.recaptcha = '';
    //     this.auth.siteMode(this.systemId, captcha).subscribe(
    //         // tslint:disable-next-line:no-shadowed-variable
    //         (data) => {
    //             this.onSuccessCheckSiteMode(data);
    //         },
    //         error => {
    //             // tslint:disable-next-line:triple-equals
    //             // this.toastr.error(error.message, 'Something want to wrong..!');
    //         });
    // }

    // async checkSiteMode() {
    //     await this.auth.siteMode(this.systemId, null).subscribe(
    //         // tslint:disable-next-line:no-shadowed-variable
    //         (data) => {
    //             this.onSuccessCheckSiteMode(data);
    //         },
    //         error => {
    //             // this.toastr.error(error.message, 'Something want to wrong..!');
    //         });
    // }

    // onSuccessCheckSiteMode(response:any) {
    //     if (response.status !== undefined && response.status === 1) {
    //         // tslint:disable-next-line:triple-equals
    //         if (response.data !== undefined && response.data.siteMode == '0') {
    //             this.sitemode = false;
    //             this.siteMaintenanceMessage = response.data.message;
    //         }
    //         if (response.data.captcha !== undefined) {
    //             this.captcha = response.data.captcha;
    //         }
    //         if (response.data.apkUrl !== undefined) {
    //             this.apkUrl = response.data.apkUrl;
    //             this.apkDownloadTag = response.data.apkName;
    //         }
    //     }
    //     this.apklink.nativeElement.setAttribute('href', this.apkUrl);
    //     this.apklink.nativeElement.setAttribute('download', this.apkDownloadTag);
    // }

    doLogin() {
        this.disableLogin = true;
        setTimeout(() => {
            const data = {
                username: this.model.username,
                password: this.model.password,
            };

            if (data.username && data.password) {
                this.auth.doLogin(data).subscribe(
                    (data) => {
                        this.intSuccessLogin(data);
                    },
                    error => {
                        // this.toastr.error(error.message, 'Something want to wrong..!');
                    });
            } else {
                this.disableLogin = false;
                if (!data.username) {
                    // this.toastr.error('Username not be null !!', 'Error:');
                } else if (!data.password) {
                    // this.toastr.error('Password not be null !!', 'Error:');
                } else {
                    // this.toastr.error('Something want to wrong..!', 'Error:');
                }
            }
        }, 2000);
    }

    intSuccessLogin(response: any) {

        this.disableLogin = false;
        if (response !== undefined && response.roleId === 1) {
            if (response !== undefined) {
                if (response) {
                    this.authIdentity.setUser(response);
                    // localStorage.setItem('currentUser', JSON.stringify(response.data));
                    window.localStorage.setItem('loadBanner', 'yes');
                    this.route.navigate(['/']);
                }
                // else {
                //     this.authId = response.data.auth_id;
                //     $('#login-form').hide();
                //     $('#change-pass-form').show();
                // }

            } else {
                // this.toastr.error(response.error.message, 'Something want wrong..!');
            }
        }
    }

    dochangepass() {
        const data = {
            username: this.model.username,
            password: this.model.password,
            password1: this.model.password1,
            password2: this.model.password2,
        };

        // this.auth.dochangepass(data).subscribe(
        //     // tslint:disable-next-line:no-shadowed-variable
        //     (data) => {
        //         this.intSuccesschangepass(data);
        //     },
        //     error => {
        //         // tslint:disable-next-line:triple-equals
        //         this.toastr.error(error.message, 'Something want to wrong..!');

        //     });
    }

    intSuccesschangepass(response: any) {
        if (response.status !== undefined && response.status === 1) {
            $('#login-form').show();
            $('#change-pass-form').hide();
            // this.reloadCaptch(null);
        }
    }

}
