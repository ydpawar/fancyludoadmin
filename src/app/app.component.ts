import { Component, HostListener, Renderer2} from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './_api/auth/login.service';
import { AuthIdentityService } from './_services/auth-identity.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  title = 'ludo';

  public isInactive = false;
  public userActivityThrottlerTimeout: any = null;
  public userActivityTimeout: any = null;
  public userActivityTimeout1: any = null;
  user: any;

  constructor(
    private auth: LoginService,
    private route: Router,
    private authIdentity: AuthIdentityService,
    private renderer: Renderer2) {

    if (authIdentity.isLoggedIn()) {
      this.user = authIdentity.getIdentity();
      // console.log(this.user);
    }
  }


  ngOnInit() {
    this.setPlatfrom();
  }

  @HostListener('window:resize', ['$event']) onResize1($event: MouseEvent) {
    this.setPlatfrom();
  }

  @HostListener('mousemove', ['$event']) onMousemove($event: MouseEvent) {
    // console.log('mousemove');
    this.userActivityThrottler();
  }

  @HostListener('scroll', ['$event']) onScroll($event: MouseEvent) {
    // console.log('scroll');
    this.userActivityThrottler();
  }

  @HostListener('keydown', ['$event']) onKeydown($event: MouseEvent) {
    this.userActivityThrottler();
  }

  @HostListener('resize', ['$event']) onResize($event: MouseEvent) {
    this.userActivityThrottler();
  }


  resetUserActivityTimeout() {
    clearTimeout(this.userActivityTimeout);

    this.userActivityTimeout = setTimeout(() => {
      this.userActivityThrottler();
      this.inactiveUserAction();
    }, 1800000);

    clearTimeout(this.userActivityTimeout1);
      this.userActivityTimeout1 = setTimeout(() => {
        this.userActivityThrottler();
        this.inactiveUser();
      }, 18000000);
  }

  userActivityThrottler() {
    if (this.isInactive) {
      this.isInactive = false;
      document.getElementsByTagName('body')[0].classList.remove('is-faded');
    }

    if (!this.userActivityThrottlerTimeout) {
      this.userActivityThrottlerTimeout = setTimeout(() => {
        this.resetUserActivityTimeout();
        clearTimeout(this.userActivityThrottlerTimeout);
        this.userActivityThrottlerTimeout = null;
      }, 10000);
    }
  }

  inactiveUserAction() {
    const isLoggedIn = (localStorage.getItem('1e846c20ec61136967c0727684fa7ed8') != null);
    if (isLoggedIn === false) {
      return null;
    }
    this.isInactive = true;
    const result = confirm('You are inactive for longtime (inactive timeout).');
    if (result) {
      // this.auth.doLogout().subscribe((res) => this.onSuccessLogout(res));
      this.onSuccessLogout();
    } else {
      window.location.reload();
    }
  }

  inactiveUser() {
    // if (this.user.role != 'ADMIN') {
    this.isInactive = true;
    // window.location.reload();
    // if(this.isInactive){
    //   const xhm5000 = setTimeout(() => { clearTimeout(xhm5000); window.location.reload();}, 5000);
    // }
    // this.auth.doLogout().subscribe((res) => this.onSuccessLogout(res));
    // this.onSuccessLogout();
    // }
  }

  onSuccessLogout() {
    this.authIdentity.logOut(this.route);
  }


  setPlatfrom() {
    if (window.innerWidth < 992) {
      this.renderer.setAttribute(document.body, 'data-platform', 'mobile');
    } else {
      this.renderer.setAttribute(document.body, 'data-platform', 'desktop');
    }
  }
}
