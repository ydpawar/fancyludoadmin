import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'app/components/commonComponent';
import { TablePoolService } from 'app/_api';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent extends BaseComponent implements OnInit {

  title = 'Tables'
  allTable: any = [];
  displayedColumns: string[] = ['name', 'entry_fee', 'pool_Price', 'play_time', 'wait_time','winner_Count' ,'allowed_Player', 'action'];
  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(inj: Injector,
    private service: TablePoolService
  ) {
    super(inj);
    this.getTable();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngDestory() {
  }

  back() {
    this.location.back();
  }

  getTable() {
    this.auth.loader_show();
    this.service.getPoolTable().subscribe((res) => this.onSuccessGetPool(res));
  }

  onSuccessGetPool(res: any) {
    if (res) {
      this.auth.loader_hide();
      this.allTable = [];
      this.allTable = res;
      this.dataSource.data = res;
    }
  }

  doDelete(pid: any, tid: any) {
    this.confirmDialog('Delete');
    this.confmDialog.subscribe((res) => {
      if (res == true) {
        this.service.deletePoolTable(pid, tid).subscribe(
          (res) => {this.onSuccessStatusUpdate(res)},
          (err) => {
            if (err.status == 400) {
              this.showNotification('Something Wrong', 'Error');
            }
          },
          ()=>{this.getTable();}
        );
      }
    });
    this.confmDialog.next(false);
  }

  onSuccessStatusUpdate(res: any) {
    if (res) {
      this.showNotification('Deleted Succesfully', 'Success');
      this.getTable();
    }
  }
}
