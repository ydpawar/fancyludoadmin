import { Routes } from '@angular/router';
import { CouponComponent } from './coupon/coupon.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TableListComponent } from './table-list/table-list.component';
import { TablePoolComponent } from './table-pool/table-pool.component';
import { TableComponent } from './table/table.component';
import { TransactionComponent } from './transaction/transaction.component';
import { TypographyComponent } from './typography/typography.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserComponent } from './user/user.component';

export const AdminLayoutRoutes: Routes = [
    { path: '',               component: DashboardComponent },
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'users',          component: UserComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'table',          component: TableComponent },
    { path: 'table-pool',     component: TablePoolComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'transaction',    component: TransactionComponent },
    { path: 'coupon',         component: CouponComponent },
];
