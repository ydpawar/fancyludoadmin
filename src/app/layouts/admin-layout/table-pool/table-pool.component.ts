import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from 'app/components/commonComponent';
import { TablePoolService } from 'app/_api';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

declare var $: any;

@Component({
  selector: 'app-table-pool',
  templateUrl: './table-pool.component.html',
  styleUrls: ['./table-pool.component.css']
})
export class TablePoolComponent extends BaseComponent implements OnInit {

  title = 'Table Pool'
  user: any;
  frm: any = FormGroup;
  frmPoolTable: any = FormGroup;
  userError: any;
  parentUser: any;
  plError: any;
  tmpplError: any;
  belance: any;
  tmpbelance: any;
  poolId: any;
  allPool: any = [];
  tableDetail: any = [];

  displayedColumns: string[] = ['name', 'action'];
  dataSource = new MatTableDataSource<any>([]);
  @ViewChild('paginator') paginator: MatPaginator;

  displayedColumnsTable: string[] = ['entry_fee', 'pool_Price', 'play_time', 'wait_time' ,'winner_Count' ,'allowed_Player'];
  dataSourceTable = new MatTableDataSource<any>([]);
  @ViewChild('paginatorTable') paginatorTable: MatPaginator;

  poolList: any[] = [
    { id: 1, name: '1 v 1 Battle', value: '1v1 Battle' },
    { id: 2, name: '3 Winners', value: '3 Winners' },
    { id: 3, name: '2 Winners', value: '2 Winners' },
    { id: 4, name: '1 Winner', value: '1 Winner' },
  ]

  constructor(
    inj: Injector,
    private service: TablePoolService
  ) {
    super(inj);
  }

  ngOnInit(): void {
    this.getPool();
    this.createForm();
  }

  createPoolTable(pId: any) {
    this.poolId = pId;
    $('.table-pool').modal('show');
    $('body').removeClass('modal-open');
  }

  viewPoolTable(item: any) {
    if(item){
      this.tableDetail = item;
      this.dataSourceTable.data = item;
      this.dataSourceTable.paginator = this.paginatorTable;
  
      $('.view-pool').modal('show');
      $('body').removeClass('modal-open');
    }
  }

  getPool() {
    // this.auth.loader_show();
    this.service.getPool().subscribe((res) => this.onSuccessGetPool(res));
  }

  onSuccessGetPool(res: any) {
    if (res) {
      // this.auth.loader_hide();
      this.allPool = [];
      this.allPool = res;
      this.dataSource.data = res;
      this.dataSource.paginator = this.paginator;
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      poolName: ['', Validators.required],
    });

    this.frmPoolTable = this.formBuilder.group({
      poolEntryAmount: ['', Validators.required],
      poolPrice: ['', Validators.required],
      waitTime: ['00', Validators.required],
      playTime: ['00', Validators.required],
      winnerCount: ['', Validators.required],
      allowedPlayerPerTable : ['', Validators.required]
    });
  }

  submitPool() {
    const data = this.frm.value;
    if (this.frm.valid) {
      this.service.createPool(data).subscribe((res) => this.onSuccess(res),
        (err) => {
          if (err) {
            this.showNotification('Something Wrong', 'Error')
            this.frm.value.poolName = '';
            $('.create-pool').modal('hide');
            $('body').removeClass('modal-open');
          }
        });
    }
  }

  submitPoolTable() {
    const pId = this.poolId
    const data = this.frmPoolTable.value;
    if (this.frmPoolTable.valid) {
      this.service.createPoolTable(pId, data).subscribe(
        (res) => this.onSuccessTable(res),
        (err) => {
          if (err) {
            this.showNotification('Something Wrong', 'Error')
            this.frmPoolTable.reset();
            $('.table-pool').modal('hide');
            $('body').removeClass('modal-open');
          }
        });
    }
  }

  onSuccess(res: any) {
    if (res) {
      this.showNotification('Created Succesfully', 'Success')
      this.frm.value.poolName = '';
      this.getPool();
      $('.create-pool').modal('hide');
      $('body').removeClass('modal-open');
    }
  }
  onSuccessTable(res: any) {
    if (res) {
      this.showNotification('Created Succesfully', 'Success')
      this.frmPoolTable.reset();
      this.getPool();
      $('.table-pool').modal('hide');
      $('body').removeClass('modal-open');
    }
  }

  back() {
    this.location.back();
  }

  poolNameChanged(e: any) {
    this.frm.value.poolName = e.target.value;
  }

  get frmPayGatName() { return this.frm.get('poolName'); }

  get frmEntryFee() { return this.frmPoolTable.get('poolEntryAmount'); }
  get frmPoolPrice() { return this.frmPoolTable.get('poolPrice'); }
  get frmWaitTime() { return this.frmPoolTable.get('waitTime'); }
  get frmPlayTime() { return this.frmPoolTable.get('playTime'); }
  get frmWinnerCount() { return this.frmPoolTable.get('winnerCount'); }
  get frmAllowPlayer() { return this.frmPoolTable.get('allowedPlayerPerTable'); }

  doDelete(pid: any) {
    this.confirmDialog('Delete');
    this.confmDialog.subscribe((res) => {
      if (res == true) {
        this.service.deletePool(pid).subscribe(
        (res) => this.onSuccessStatusUpdate(res),
        (err) => {
          if (err.status == 400) {
            this.showNotification('Something Wrong', 'Error');
          }
        }
        );
        this.getPool();
      }
    }, );
    this.confmDialog.next(false);
  }

  onSuccessStatusUpdate(res: any) {
    if (res) {
      this.showNotification('Deleted Succesfully', 'Success');
      this.getPool();
    }
  }

}
