import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from 'app/components/commonComponent';
import { CouponService } from 'app/_api/coupon/coupon.service';

declare var $: any;

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.css']
})
export class CouponComponent extends BaseComponent implements OnInit {

  title = 'Coupon'
  allCoupon: any = [];
  frm: any = FormGroup;

  displayedColumns: string[] = ['code', 'couponId', 'desc', 'expiredAt', 'action'];
  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(inj: Injector,
    private service: CouponService
  ){
    super(inj);
    this.createForm();
  }

  ngOnInit(): void {
    this.getCoupon();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  back() {
    this.location.back();
  }

  getCoupon() {
    this.auth.loader_show();
    this.service.getCoupon().subscribe((res) => this.onSuccessCoupon(res));
  }

  onSuccessCoupon(res: any) {
    if (res) {
      this.auth.loader_hide();
      this.allCoupon = [];
      this.allCoupon = res;
      this.dataSource.data = res;
    }
  }

  createCoupon(pId: any) {
    // this.poolId = pId;
    $('.create-coupon').modal('show');
    $('body').removeClass('modal-open');
  }

  createForm() {
    this.frm = this.formBuilder.group({
      code: ['', Validators.required],
      desc: ['', Validators.required],
      expiredAt: ['', Validators.required]
    });
  }

  submitCoupon() {
    // const pId = this.poolId
    const data = this.frm.value;
    if (this.frm.valid) {
      this.service.createCoupon(data).subscribe(
        (res) => this.onSuccess(res),
        (err) => {
          if (err) {
            this.showNotification('Something Wrong', 'Error')
            this.frm.reset();
            $('.create-coupon').modal('hide');
            $('body').removeClass('modal-open');
          }
        });
    }
  }

  onSuccess(res: any) {
    if (res) {
      this.showNotification('Created Succesfully', 'Success')
      this.getCoupon();
      $('.create-coupon').modal('hide');
      $('body').removeClass('modal-open');
    }
  }

  get frmCode() { return this.frm.get('code'); }
  get frmDesc() { return this.frm.get('desc'); }
  get frmExpiredAt() { return this.frm.get('expiredAt'); }

  doDelete(cId: any) {
    this.confirmDialog('Delete');
    this.confmDialog.subscribe((res) => {
      if (res == true) {
        this.service.deleteCoupon(cId).subscribe(
          (res) => {this.onSuccessDelete(res)},
          (err) => {
            if (err.status == 400) {
              this.showNotification('Something Wrong', 'Error');
            }
          },
          ()=>{this.getCoupon();}
        );
      }
    });
    this.confmDialog.next(false);
  }

  onSuccessDelete(res: any) {
    if (res) {
      this.showNotification('Deleted Succesfully', 'Success');
      this.getCoupon();
    }
  }

}
