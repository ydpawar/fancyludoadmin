import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'app/components/commonComponent';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent extends BaseComponent implements OnInit {

  title = 'Transaction'

  constructor(inj: Injector,
    ){
      super(inj);
    }
  
    ngOnInit(): void {
    }
  
    back() {
      this.location.back();
    }

}
