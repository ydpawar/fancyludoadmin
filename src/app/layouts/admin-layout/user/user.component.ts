import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BaseComponent } from 'app/components/commonComponent';
import { UsersService } from 'app/_api';

declare var $: any;
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent extends BaseComponent implements OnInit {

  title = 'Users'
  displayedColumns: string[] = ['userId', 'name', 'mobile', 'email', 'action'];
  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  bankDetail: any;
  kycDetail: any;
  profileDeatil: any;

  imageData: String = '';

  constructor(inj: Injector,
    private service: UsersService
  ) {
    super(inj);
    this.getUsers();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngDestory() {
  }

  viewBankDetail(item: any) {
    if (item) {
      this.bankDetail = item;
      $('.view-bankDetail').modal('show');
      $('body').removeClass('modal-open');
    }
  }

  viewKycDetail(item: any) {
    if (item) {
      this.kycDetail = item;
      $('.view-KYCDetail').modal('show');
      $('body').removeClass('modal-open');
    }
  }

  viewProfile(uId,imgName) {
    if(uId && imgName){
    this.imageData = 'http://ec2-65-1-0-48.ap-south-1.compute.amazonaws.com:5000/fancyludo/api/user/' + uId +'/download/'+ imgName ;
    $('.view-profile').modal('show');
    $('body').removeClass('modal-open');
    }
  }

  back() {
    this.location.back();
  }

  getUsers() {
    this.auth.loader_show();
    this.service.getUsers().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res: any) {
    if (res) {
      this.auth.loader_hide();
      this.dataSource.data = res;
      // const xhm5000 = setTimeout(() => { clearTimeout(xhm5000); window.location.reload();}, 10000);
    }
  }

  doDelete(uId: any) {
    this.confirmDialog('Delete');
    this.confmDialog.subscribe((res) => {
      if (res == true) {
        this.service.deleteUsers(uId).subscribe(
          (res) => { this.onSuccessStatusUpdate(res) },
          (err) => {
            if (err.status == 400) {
              this.showNotification('Something Wrong', 'Error');
            }
          },
          () => { this.getUsers(); }
        );
      }
    });
    this.confmDialog.next(false);
  }

  onSuccessStatusUpdate(res: any) {
    if (res) {
      this.showNotification('Deleted Succesfully', 'Success');
      this.getUsers();
    }
  }
}
