import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'app/components/commonComponent';
declare var $: any;
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent extends BaseComponent implements OnInit {

  title = 'Notifications'

  constructor(inj: Injector,
  ) {
    super(inj);
  }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }
}
