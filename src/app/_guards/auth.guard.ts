import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, CanActivateChild } from "@angular/router";
import { Observable } from "rxjs";
import { AuthIdentityService, User } from './../_services/index';

// import { Md5 } from 'ts-md5/dist/md5';

export class CheckPermissions {
    auth: AuthIdentityService;
    user:any = User;

    constructor(private _router: Router) {
        this.auth = new AuthIdentityService();
        // if (this.auth.isLoggedIn) {
        //     this.user = this.auth.getIdentity();
        // }
    }

    init(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.checkPermission(route, state);
    }

    checkPermission(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let isAllowed: boolean = false;

        if (this.auth.isLoggedIn()) {
            isAllowed = true;
        }

        if (!isAllowed) {
            this.logOut();
        }

        return isAllowed;
    }

    hasPermission(url: string): boolean {
        let isAllowed: boolean = false;

        if (this.auth.isLoggedIn()) {
            isAllowed = true;
        }

        return isAllowed;
    }

    getUrl(url:any): string {
        let rUrl = '';
        let urlArr = url.split('/');

        if (urlArr.length > 2) {
            if (urlArr[urlArr.length - 2] == 'update' || urlArr[urlArr.length - 2] == 'view') {
                urlArr.pop();
            }
        }

        rUrl = urlArr.join('/');
        rUrl = this.trim(rUrl);
        // rUrl = Md5.hashStr(rUrl + 'iConsentCMS').toString();

        return rUrl;
    }

    trim(s:any) {
        s = s.replace(/(^\/)/, ""); s = s.replace(/\/$/, "");
        return s;
    }

    logOut() {
        this.auth.logOut();
        this._router.navigate(['/login']);
    }
}

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private _router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let cPermission = new CheckPermissions(this._router);
        return cPermission.init(route, state);
    }
}

@Injectable()
export class AuthGuardChild implements CanActivateChild {
    constructor(private _router: Router) {
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let cPermission = new CheckPermissions(this._router);
        return cPermission.init(route, state);
    }
}

@Injectable()
export class CanLoginActivate implements CanActivate {
    constructor(private router: Router,private auth: AuthIdentityService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // console.log('can logout activate called', this.auth.isLoggedIn());
        if (!this.auth.isLoggedIn()) {
            // console.log('returning true', this.auth.isLoggedIn());
            return true;

        }
        this.router.navigate(['/']);
        return false;
    }
}

@Injectable()
export class CanAuthActivate implements CanActivate {
    constructor(private router: Router, private auth: AuthIdentityService) {
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.auth.isLoggedIn()) {
            return true;
        }

        // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        this.router.navigate(['/login']);
        return false;
    }
}
