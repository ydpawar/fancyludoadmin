import { HttpHeaderResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from './../../_services/index';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  baseUrl = ''
  constructor(private http: HttpService) {
    this.baseUrl = this.http.baseUrl;
  }

  getUsers(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'user/';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  deleteUsers(uId:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'user/' + uId;
    httpRequestModel.method = 'DELETE';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse();

    return this.http.init(httpRequestModel, processDisplay,httpHeaderResponse);
  }


}
