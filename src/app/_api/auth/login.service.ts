import { HttpHeaderResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from './../../_services/index';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpService) { }

  // siteMode(optId, captcha): Observable<JSON> {
  //   const httpRequestModel = new HttpRequestModel();
  //   if ( captcha != null ) {
  //     httpRequestModel.url = 'site-mode?captcha=' + captcha + '&optId=' + optId;
  //   } else {
  //     httpRequestModel.url = 'site-mode?optId=' + optId;
  //   }
  //   httpRequestModel.method = 'GET';
  //   httpRequestModel.body = {};
  //   httpRequestModel.header = 'Auth';

  //   const processDisplay = new HttpRequestProcessDisplay();
  //   processDisplay.loader = true;
  //   processDisplay.loaderMessage = 'Validating User!';
  //   processDisplay.responseMessage = false;

  //   return this.http.init(httpRequestModel, processDisplay);
  // }

  doLogin(data:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'auth/admin/login';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'NoAuth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse();

    return this.http.init(httpRequestModel, processDisplay ,httpHeaderResponse);
  }

  dochangepass(data:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'change-password';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'NoAuth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse();

    return this.http.init(httpRequestModel, processDisplay ,httpHeaderResponse);
  }

  doLogout(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'logout';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = false;

    const httpHeaderResponse = new HttpHeaderResponse();

    return this.http.init(httpRequestModel, processDisplay ,httpHeaderResponse);
  }
}
