import { HttpHeaderResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from './../../_services/index';

@Injectable({
  providedIn: 'root'
})
export class CouponService {

  constructor(private http: HttpService) { }

  getCoupon(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'coupon';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  createCoupon(data:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'coupon';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse(); 

    return this.http.init(httpRequestModel, processDisplay ,httpHeaderResponse);
  }

  UpdateCoupon(couponId:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'coupon/' + couponId;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse(); 

    return this.http.init(httpRequestModel, processDisplay ,httpHeaderResponse);
  }


  deleteCoupon(couponId:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'coupon/' + couponId;
    httpRequestModel.method = 'DELETE';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse();

    return this.http.init(httpRequestModel, processDisplay,httpHeaderResponse);
  }

  statusCoupon(couponId){
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'pool/' + couponId;
    httpRequestModel.method = 'DELETE';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse();

    return this.http.init(httpRequestModel, processDisplay,httpHeaderResponse);
  }

}
