import { HttpHeaderResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from './../../_services/index';

@Injectable({
  providedIn: 'root'
})
export class TablePoolService {

  constructor(private http: HttpService) { }

  createPool(data:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'pool';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse(); 

    return this.http.init(httpRequestModel, processDisplay ,httpHeaderResponse);
  }

  createPoolTable(pId:any,data:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'pool/'+ pId +'/table';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse(); 

    return this.http.init(httpRequestModel, processDisplay ,httpHeaderResponse);
  }

  getPool(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'pool';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getPoolTable(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'pool/tables';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  deletePool(poolId:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'pool/' + poolId;
    httpRequestModel.method = 'DELETE';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse();

    return this.http.init(httpRequestModel, processDisplay,httpHeaderResponse);
  }

  deletePoolTable(pId:any,tId:any): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'pool/' + pId +'/table/'+ tId;
    httpRequestModel.method = 'DELETE';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = true;

    const httpHeaderResponse = new HttpHeaderResponse(); 

    return this.http.init(httpRequestModel, processDisplay,httpHeaderResponse);
  }

}
